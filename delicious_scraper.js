function loadJQuery(){
    script = document.createElement('script');
    script.setAttribute('src', '//code.jquery.com/jquery-2.1.3.js');
    script.setAttribute('type', 'text/javascript');
    script.onload = loadSaveAs;
    document.getElementsByTagName('head')[0].appendChild(script);
}

function loadSaveAs(){
    saveAsScript = document.createElement('script');
    saveAsScript.setAttribute('src', 'https://rawgit.com/eligrey/FileSaver.js/master/FileSaver.js');
    saveAsScript.setAttribute('type', 'text/javascript');
    saveAsScript.onload = saveToFile;
    document.getElementsByTagName('head')[0].appendChild(saveAsScript);
}

function saveToFile() {
    var result = [];
    var allLinks = $('.link').children().each(function() {
        var bookmark_title = $(this).children('.title-wrapper').children().html();
        var bookmark_url = $(this).children('.title-wrapper').children().attr('href');
        var bookmark_date = $(this).children('.meta.meta-secondary').children('time.date').html();
        var tags = [];
        var bookmark_tags = $(this).children('.meta').children('ul.tags').children('li.dropdown').children('ul.dropdown-menu').each(function() {
            var tag = $(this).attr('data-tag');
            if(tag) {
                tags.push(tag);
            }
        });
        var bookmark_notes = $(this).children('.note').html();
        if(!bookmark_notes) {
            bookmark_notes = '';
        }

        result.push({
            title: bookmark_title,
            url: bookmark_url,
            date: bookmark_date,
            tags: tags,
            notes: bookmark_notes,
        });
    });

    // Convert to a nicely indented JSON string
    json = JSON.stringify(result);
    console.log(json);
    var blob = new Blob([json], {type: "text/plain;charset=utf-8"});
    saveAs(blob, "FeedlySavedForLater" + Date.now().toString() + ".txt");
}

loadJQuery();
