# README #

I had a bunch of links in my Delicious account and had no access, so I wrote this scraper to get the link data (title, url, date, tags, and notes). Paste the code into your browser developer console, run and enjoy.

* If you intend to download every link off a Delicious page, you will have to scroll down until all of your links are displayed on the page before running.

Influenced by: https://github.com/ShockwaveNN/pocket-sinatra-interface/blob/master/feedly_export_saved_for_later.js